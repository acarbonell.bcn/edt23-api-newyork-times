package com.example.edt23;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.edt23.model.Result;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText textSearch;
    private ImageButton btnSearch;
    private RecyclerView rView;
    private String url;
    private TextView textStatus;
    private TextView textResults;
    MyAdapter myAdapter;
    //private List<Result> results;
    public List<Result> results =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textSearch = findViewById(R.id.textSearch);
        btnSearch = findViewById(R.id.btnSearch);
        rView = findViewById(R.id.rView);
        textStatus  = findViewById(R.id.textStatus);
        textResults = findViewById(R.id.textNumResults);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if (!results.isEmpty()) results.clear();
                url = "https://api.nytimes.com/svc/movies/v2/reviews/search.json?query=" + textSearch.getText().toString()+ "&api-key=AAgW7NJTPaQ1hJ3USOsSbQ9Uk0AoOZ4z";
                getResult();
            }
        });
    }

    public void getResult(){
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.optString("status");
                            String num_results = response.optString("num_results");
                            textStatus.setText(response.optString("status"));
                            textResults.setText(response.optString("num_results"));
                            JSONArray jsonArray = response.optJSONArray("results");
                            results = Arrays.asList( new GsonBuilder().create().fromJson(jsonArray.toString(),
                                        Result[].class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        rView.setAlpha(1);
                        rView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), results);
                        rView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }
}