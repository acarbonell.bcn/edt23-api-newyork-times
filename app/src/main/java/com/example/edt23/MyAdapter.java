package com.example.edt23;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edt23.model.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context context;
    private List<Result>  result;

    public MyAdapter(Context context, List<Result> result) {
        this.context = context;
        this.result = result;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =  LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textTitle.setText(result.get(position).getDisplay_title());
        holder.textByline.setText(result.get(position).getByline());
        holder.textheadline.setText(result.get(position).getHeadline());
        holder.textPubli.setText(result.get(position).getPublication_date());
        if (result.get(position).getMultimedia() != null){
            Picasso.get().load(result.get(position).getMultimedia().getSrc()).fit().centerCrop().into(holder.imgResult);
        } else {
            holder.imgResult.setImageResource(R.drawable.ic_image_not_supported);
        }
        holder.rowLayout.setOnClickListener(v ->{
            Intent i = new Intent(context, DetailActivity.class);
            i.putExtra("img", result.get(holder.getAdapterPosition()).getMultimedia().getSrc());
            i.putExtra("title", result.get(holder.getAdapterPosition()).getDisplay_title());
            i.putExtra("by", result.get(holder.getAdapterPosition()).getByline());
            i.putExtra("head", result.get(holder.getAdapterPosition()).getHeadline());
            i.putExtra("date", result.get(holder.getAdapterPosition()).getPublication_date());
            i.putExtra("summary", result.get(holder.getAdapterPosition()).getSummary_short());
            i.putExtra("btn", result.get(holder.getAdapterPosition()).getLink().getUrl());
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        });
        /*holder.btnLink.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse(result.get(holder.getAdapterPosition()).getLink().getUrl()));
            context.startActivity(intent);
        });*/
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textTitle;
        TextView textByline;
        TextView textheadline;
        TextView textPubli;
        ImageView imgResult;
        ConstraintLayout rowLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            textTitle = itemView.findViewById(R.id.textTitle);
            textByline = itemView.findViewById(R.id.textByline);
            textheadline = itemView.findViewById(R.id.textheadline);
            textPubli = itemView.findViewById(R.id.textPublication);
            imgResult = itemView.findViewById(R.id.imgMovie);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
