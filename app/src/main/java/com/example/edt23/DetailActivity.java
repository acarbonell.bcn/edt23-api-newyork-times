package com.example.edt23;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private ImageView imgDetail;
    private TextView detailTite;
    private TextView detailBy;
    private TextView detailHead;
    private TextView detailDate;
    private TextView detailSummary;
    private Button btnDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imgDetail = findViewById(R.id.imgDetail);
        detailTite = findViewById(R.id.detailTitle);
        detailBy = findViewById(R.id.detailBy);
        detailHead = findViewById(R.id.detailHead);
        detailDate = findViewById(R.id.detailDate);
        detailSummary = findViewById(R.id.detailSummary);
        btnDetail = findViewById(R.id.btnDetail);

        Picasso.get().load(getIntent().getStringExtra("img")).fit().centerCrop().into(imgDetail);

        detailTite.setText(getIntent().getStringExtra("title"));
        detailBy.setText(getIntent().getStringExtra("by"));
        detailHead.setText(getIntent().getStringExtra("head"));
        detailDate.setText(getIntent().getStringExtra("date"));
        detailSummary.setText(getIntent().getStringExtra("summary"));

        btnDetail.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse(getIntent().getStringExtra("btn")));
            startActivity(intent);
        });
    }
}